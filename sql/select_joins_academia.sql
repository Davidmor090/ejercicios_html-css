USE academiax2_tarde;

SELECT c.descripcion, a.nombre
FROM cursos c 
JOIN ediciones e
ON e.cursos_idcursos = c.idcursos
JOIN matricula m
ON m.ediciones_idediciones = e.idediciones
JOIN alumnos a
ON a.idalumnos = m.alumnos_idalumnos
WHERE e.idediciones = 9;

#####################################################################

SELECT c.descripcion, o.modelo, o.marca, o.numero, a.nombre
FROM ordenadores o
JOIN matricula m
ON m.ordenadores_idordenadores = o.idordenadores
JOIN alumnos a
ON a.idalumnos = m.alumnos_idalumnos
JOIN ediciones e
ON e.idediciones = m.ediciones_idediciones
JOIN cursos c
ON c.idcursos = e.cursos_idcursos;

####################################################################

SELECT p.nombre nombre_profesor, a.nombre nombre_alumno
FROM alumnos a
JOIN matricula m
ON m.alumnos_idalumnos = a.idalumnos
JOIN ediciones e
ON e.idediciones = m.ediciones_idediciones
JOIN profesores p
ON p.idprofesores = e.profesores_idprofesores;

###################################################################

SELECT c.lenguaje, a.nombre, o.marca
FROM ordenadores o
JOIN matricula m
ON m.ordenadores_idordenadores = o.idordenadores
JOIN alumnos a
ON a.idalumnos = m.alumnos_idalumnos
JOIN ediciones e
ON e.idediciones = m.ediciones_idediciones
JOIN cursos c
ON c.idcursos = e.cursos_idcursos;

###################################################################

SELECT c.descripcion, e.finicio, count(a.idalumnos) cantidad_alumnos
FROM ordenadores o
JOIN matricula m
ON m.ordenadores_idordenadores = o.idordenadores
JOIN alumnos a
ON a.idalumnos = m.alumnos_idalumnos
JOIN ediciones e
ON e.idediciones = m.ediciones_idediciones
JOIN cursos c
ON c.idcursos = e.cursos_idcursos
GROUP BY c.idcursos;

