use vgames_tarda;

/*Recuento total de registros de ventas (número de registros de la tabla)*/

SELECT count(Global_Sales) as registros_total_ventas
FROM fullcsv;

/*Distintos tipos de plataformas (relación)*/

SELECT distinct(platform)
FROM fullcsv;

/*Número de plataformas distintas (número de registros)*/

SELECT count(distinct(platform))
FROM fullcsv;

/*Suma de ventas totales para la plataforma “PSP” (cantidad en M$)*/

SELECT sum(global_sales) as ventas_totales
FROM fullcsv
WHERE platform like "PSP";

/*Suma de ventas totales en europa, norte américa y japón (cantidad en M$)*/

SELECT sum(NA_Sales)+sum(EU_Sales)+sum(JP_Sales) as ventas_NA_EU_JP
FROM fullcsv;

/*Suponiendo la población actual, el "continente" con mayor índice de ventas por cápita*/

SELECT sum(NA_Sales)/350.7 as NA_VXC, sum(EU_Sales)/741.4 as EU_VXC, sum(JP_Sales)/126.8 as JP_VXC
FROM fullcsv;

/*La suma de ventas en cada continente, solo para los géneros: racing, sports y simulation*/

SELECT Genre, sum(NA_Sales), sum(EU_Sales), sum(JP_Sales)
FROM fullcsv
WHERE Genre in ("racing", "sports", "simulation")
GROUP BY Genre;

/*Suma total de ventas por plataforma en los años 2008-2010*/

SELECT platform, sum(global_sales), Year_of_Release
FROM fullcsv
WHERE Year_of_Release in (2008, 2009, 2010)
GROUP BY platform;

/*Suma de ventas por año para el género “Sports”*/

SELECT platform, genre, sum(global_sales), Year_of_Release
FROM fullcsv
WHERE genre in ("sports")
GROUP BY Year_of_release;

/*Suma de ventas por año para el género “strategy”*/

SELECT platform, genre, sum(global_sales), Year_of_Release
FROM fullcsv
WHERE genre in ("strategy")
GROUP BY Year_of_release;

/*La suma de ventas en cada continente, solo para los géneros: racing, sports y
simulation (los 3 sumados) SEPARANDO LOS 3 GÉNEROS*/

SELECT Genre, sum(NA_Sales), sum(EU_Sales), sum(JP_Sales)
FROM fullcsv
WHERE Genre in ("racing", "sports", "simulation")
GROUP BY Genre;

/*Los 10 juegos más vendidos de la historia, en volumen de ventas totales (eu+na+jp)*/

SELECT name, (NA_Sales+EU_Sales+JP_Sales) as ventas_NA_EU_JP
FROM fullcsv
ORDER BY ventas_NA_EU_JP desc limit 10;

/*El año en que se vendieron más videojuegos de estrategia*/

SELECT Year_of_Release, global_sales
FROM fullcsv
WHERE genre like "strategy"
GROUP BY Year_of_release
ORDER BY global_sales desc limit 1;

/*Los 10 juegos más vendidos de la historia, en volumen de ventas totales (eu+na+jp)
○ columnas: titulo, nombre de género*/

SELECT name, genre, round(NA_Sales+EU_Sales+JP_Sales) as ventas_NA_EU_JP
FROM fullcsv
ORDER BY ventas_NA_EU_JP desc limit 10;

/*Mostrar las 5 plataformas con mayor número de ventas en período 2010-2015
○ columnas: nombre de plataforma, suma de ventas total (eu+na+jp)*/

SELECT platform, global_sales
FROM fullcsv
WHERE year_of_release between 2010 and 2015
GROUP BY platform
ORDER BY global_sales desc limit 5;

/*Mostrar los 5 publishers con mayor número de ventas en período 2010-2015
○ columnas: nombre de publisher, suma de ventas total (eu+na+jp)*/

SELECT publisher, global_sales
FROM fullcsv
WHERE year_of_release between 2010 and 2015
GROUP BY publisher
ORDER BY global_sales desc limit 5;

/*Mostrar evolución de ventas para las playstation en todos los años
○ columnas: año, nombre de plataforma, suma de ventas total (eu+na+jp)
○ orden ascendente por año
○ plataformas: PS, PS2, PS3, PS4*/

SELECT Year_of_Release, platform, round(sum(global_sales)) as total_ventas
FROM fullcsv
WHERE platform in ("ps", "ps2", "ps3", "ps4")
GROUP BY year_of_release, platform
ORDER BY year_of_release asc;

/*El año en que se vendieron más juegos de "pokemon" (en todas sus variantes)*/

SELECT Year_of_Release, round(sum(Global_Sales)) total
FROM fullcsv
WHERE name LIKE "%pokemon%"
GROUP BY Year_of_Release
ORDER BY total desc limit 1








