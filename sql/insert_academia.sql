USE academiax2_tarde;

INSERT INTO profesores (nombre) 
VALUES ("ricard"), ("adria"), ("eric");

SELECT *
FROM matricula;

INSERT INTO tutor (nombre)
VALUES ("sole"), ("eric");

INSERT INTO cursos (descripcion, lenguaje, horas)
VALUES ("Programacion web con Java", "java", 275), ("Programacion web con .NET", ".NET", 250), ("Programacion web con Phyton", "Phyton", 250);

INSERT INTO ediciones (cursos_idcursos, matitarda, finicio, profesores_idprofesores, tutor_idtutor)
VALUES (3, 2, "2019-02-18", 5, 4);

INSERT INTO ediciones (cursos_idcursos, matitarda, finicio, profesores_idprofesores, tutor_idtutor)
VALUES (4, 1, "2019-03-11", 2, 3);

INSERT INTO alumnos (nombre, email)
VALUES ("david","david@gmail.com"), ("juanka","juanka@gmail.com"), ("martin","martin@gmail.com"), ("melanie","melanie@gmail.com"), ("jordi","jordi@gmail.com"), ("edgar","edgar@gmail.com"),
		("thais","thais@gmail.com"), ("luismiguel","luismiguel@gmail.com"), ("toni","toni@gmail.com"), ("marc","marc@gmail.com"), ("manolo","manolo@gmail.com"), ("sebas","sebas@gmail.com"),
        ("pep","pep@gmail.com"), ("aznar","aznar@gmail.com"), ("jonnieve","jonnieve@gmail.com");

INSERT INTO ordenadores (modelo, marca, anyo_compra, numero)
VALUES ("maquinon","lenovo","2018-12-11",13), ("maquinon","lenovo","2018-12-11",14), ("maquinon","dell","2018-12-11",15), ("maquinon","hp","2018-12-11",16), ("maquinon","lenovo","2018-12-11",17), 
		("maquinon","hp","2018-12-11",18), ("maquinon","dell","2018-12-11",19), ("maquinon","lenovo","2018-12-11",20), ("maquinon","dell","2018-12-11",21),
		("maquinon","lenovo","2018-12-11",22), ("maquinon","mac","2018-12-11",23), ("maquinon","mac","2018-12-11",24), ("maquinon","mac","2018-12-11",25), ("maquinon","mac","2018-12-11",26);
        
INSERT INTO ordenadores (modelo, marca, anyo_compra, numero)        
VALUES   ("maquinon","hp","2018-12-11",27);

INSERT INTO matricula (ediciones_idediciones, alumnos_idalumnos, ordenadores_idordenadores)
VALUES (5,20,41), (5,21,42), (5,22,43), (5,23,44), (5,24,45), (9,25,46), (9,26,47), (9,26,48), (9,27,49), (9,28,50);